package main

import (
	"log"

	"github.com/streadway/amqp"
)

func main() {
	// dial the rabbit server
	conn, err := amqp.Dial("amqp://guest:guest@localhost:8081")
	if err != nil {
		log.Panic(err)
	}

	// define a channel
	ch, err := conn.Channel()
	if err != nil {
		log.Panic(err)
	}

	// define a queue
	q, _ := ch.QueueDeclare(
		"first.queue", // name string
		true,          //durable bool
		false,         //autoDelete bool
		false,         //exlusive bool
		false,         // noWait bool
		nil,           //args qamqp.Table
	)

	// // find to a particular exchange, other than default
	// ch.QueueBind(
	// 	q.Name,       // name string
	// 	"",           // key string
	// 	"amq.fanout", // exchange string
	// 	false,        // noWait bool
	// 	nil,          //args amqp.Table
	// )

	// message to publish
	msg := amqp.Publishing{
		Body: []byte("my first message"),
	}

	// params for the message
	ch.Publish(
		"",
		q.Name,
		false,
		false,
		msg,
	)

	// define a consumer
	msgs, _ := ch.Consume(
		q.Name, // queue name
		"",     // consumer
		true,   // autoAck
		false,  // exclusive
		false,  // noLocal
		false,  //noWait
		nil,    //args, amqp.Table
	)

	// range over the channel and print the contents
	for m := range msgs {
		println(string(m.Body))
	}

}
